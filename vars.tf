variable "environment" {
  description = "environment where this resource is used"
  type        = string
}

variable "zone_id" {
  description = "id of the dns zone"
  type        = string
}

variable "allow_overwrite" {
  description = "allow overwrite this record if exists"
  type        = bool
}

variable "record" {
  description = "domain record to register"
  type        = object({
    name   = string,
    type   = string,
    ttl    = string,
    values = list(string),
  })
}