resource "aws_route53_record" "default" {
  allow_overwrite = var.allow_overwrite
  zone_id         = var.zone_id
  name            = var.record.name
  type            = var.record.type
  ttl             = var.record.ttl

  records = var.record.values
}